const express = require("express");

const api = require("./api");
const pages = require("./pages");

function routes(app) {
  const routers = {
    api: express.Router(),
    public: express.Router()
  };

  api(routers);
  pages(routers);

  app.use("/api", routers.api);
}

module.exports = routes;
