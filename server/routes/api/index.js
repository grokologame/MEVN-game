const user = require("../../controllers/user");

function api(routers) {
  routers.api.post("/register", user.create);
  routers.api.post("/login", user.login);
}

module.exports = api;
