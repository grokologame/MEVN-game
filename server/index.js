const appRoot = require("app-root-path");
const express = require("express");
const dotenv = require("dotenv");
const bodyParser = require("body-parser");
const passport = require("passport");
const connectDB = require("./config/db");
const routes = require("./routes");

// Check for sane env file
const envReturn = dotenv.config({ path: "server/config/config.env" });
if (envReturn.error) {
  console.log("Something wrong with .env? ", envReturn.error);
}

connectDB();
const app = express();
app.use(
  bodyParser.urlencoded({
    extended: false
  })
);
app.use(bodyParser.json());
app.use(express.static(`${appRoot}/public`));
app.use(express.static(`${appRoot}/public/dist`));
app.use(passport.initialize());
require("./config/passport")(passport);

routes(app);

// Start Server
const PORT = process.env.PORT;
app.listen(PORT);

if (process.env.NODE_ENV === "development") {
  console.log(`Server running on: localhost:${PORT}`);
} else {
  console.log("prod");
}

module.exports = app;
